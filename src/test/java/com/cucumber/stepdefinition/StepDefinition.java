package com.cucumber.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.cucumber.Baseclass.BaseClass;
import com.cucumber.runner.TestRunner;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition extends BaseClass {

	public static WebDriver driver = TestRunner.driver;

	@Given("User is on facebook application page")
	public void user_is_on_facebook_application_page() {
		passurl("http://adactin.com/HotelApp/index.php");
		
	}

	@When("User enters valid username")
	public void user_enters_valid_username() {
		WebElement username = driver.findElement(By.xpath("//*[@id='username']"));
		inputdata(username, "vinothmp9");
	}

	@When("User enters valid password")
	public void user_enters_valid_password() {

		WebElement password = driver.findElement(By.xpath("//*[@id='password']"));
		BaseClass.inputdata(password, "vinothvinoth"); 
	}

	@When("User Click on the sign button")
	public void user_Click_on_the_sign_button() {
		WebElement loginBtn = driver.findElement(By.xpath("//*[@id='login']"));
		BaseClass.clickButton(loginBtn);
	}

	@When("User select hotel")
	public void user_select_hotel() {
		WebElement location = driver.findElement(By.xpath("//*[@id='location']"));
		BaseClass.ddlb_Select(location, "value", "Melbourne");
	}

	@When("User Select location")
	public void user_Select_location() {
		WebElement hotels = driver.findElement(By.xpath("//*[@id='hotels']"));
		BaseClass.ddlb_Select(hotels, "value","Hotel Creek" );
	}

	@When("User Select roomtype")
	public void user_Select_roomtype() {
		WebElement room_type = driver.findElement(By.xpath("//*[@id='room_type']"));
		BaseClass.ddlb_Select(room_type, "value", "Standard");
	}

	@When("User select NO of rooms")
	public void user_select_NO_of_rooms() {
		WebElement room_nos = driver.findElement(By.xpath("//*[@id='room_nos']"));
		BaseClass.ddlb_Select(room_nos, "value", "2");
	}

	@Then("User Verify the Username in the homepage")
	public void user_Verify_the_Username_in_the_homepage() {
		WebElement verify = driver.findElement(By.xpath("//*[@id='username_show']"));
		if(verify.getAttribute("value").contains("vinothmp9")) {
			System.out.println("valid page");
		}
		else {
			System.out.println("invalid page");
		}
	}

}
